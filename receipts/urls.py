from django.urls import path
from receipts.views import (
    ReceiptListView,
    AccountListView,
    ExpenseCategoryListView, 
    AccountCreateView,
    CategoryCreateView,
    ReceiptCreateView,
)



urlpatterns = [
    path("", ReceiptListView.as_view(), name="home"),
    path("accounts/", AccountListView.as_view(), name="accounts_list"),
    path("categories/", ExpenseCategoryListView.as_view(), name="categories_list"),
    path("accounts/create/", AccountCreateView.as_view(), name="accounts_create"),
    path("categories/create/", CategoryCreateView.as_view(), name="categories_create"),
    path("create/", ReceiptCreateView.as_view(), name="receipts_create"),
]
